import { A, Route, Routes } from '@solidjs/router';
import { type Component } from 'solid-js';
import { Feed } from './pages/Feed';
import { Users } from './pages/Users';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { CalendarIcon, ChatBubble, ChevronRightIcon, DocumentCheckIcon, FolderOpenIcon, HomeIcon, InboxIcon, ListBulletIcon, PlusIcon, RocketLaunchIcon, SearchIcon, SettingsIcon, UsersIcon } from './components/Icons';
import { NavButton, NavLink } from './components/NavItems';
import { Chat } from './pages/Chat';

const Header: Component = () => {
  return (
    <header class="border-b dark:border-gray-700 w-full flex">
      <nav class="mx-2 flex items-center space-x-6 w-full">
        <A href="#" class='text-lg font-bold p-3'>
          LH
        </A>
        <Breadcrumbs class='flex-1 p-3' />
        <div class="flex items-center justify-between space-x-3 p-3">
          <NavButton func={()=>{}} icon={PlusIcon} name='Add' class='hidden md:block' />
          <NavButton func={()=>{}} icon={SearchIcon} name='Search' class='hidden md:block' />
          <NavLink href='/feed' icon={InboxIcon} name='Feed' />
          <A href="#" class=''>
            Timon Förster
          </A>
        </div>
      </nav>
    </header>
  );
}

const Breadcrumbs: Component<{class?: string}> = (props: {class: string}) => {
  return (
    <div class={`flex items-center overflow-x-auto whitespace-nowrap space-x-3 text-sm ${props.class}`}>
      <A href="/" class="flex items-center -px-2">
        <HomeIcon class='w-4 h-4 mr-2' />
        OrganisationABC
      </A>
      <span class="text-gray-500 dark:text-gray-300 rtl:-scale-x-100">
        <ChevronRightIcon class='w-5 h-5' />
      </span>
      <A href="/" class="flex items-center -px-2">
        <UsersIcon class='w-4 h-4 mr-2' />
        Group1
      </A>
      <span class="text-gray-500 dark:text-gray-300 rtl:-scale-x-100">
        <ChevronRightIcon class='w-5 h-5' />
      </span>
      <A href="/" class="flex items-center -px-2 font-bold">
        <RocketLaunchIcon class='w-4 h-4 mr-2' />
        ProjectX
      </A>
    </div>
  );
}

const Sidebar: Component = () => {
  return(
    <aside class="w-16 border-r rtl:border-l rtl:border-r-0 dark:border-gray-700">
      <nav class="flex flex-col h-full">
        <div class="flex flex-col flex-1 space-y-3 mt-3 items-center overflow-y-auto">
          <NavLink href='/group1' icon={ListBulletIcon} name='Group Details' />
          <NavLink href='/group1/chat' icon={ChatBubble} name='Chat' />
          <NavLink href='/group1/tasks' icon={DocumentCheckIcon} name='Tasks' />
          <NavLink href='/group1/pages' icon={FolderOpenIcon} name='Pages' />
        </div>
        <div class="flex flex-col space-y-3 mb-3 items-center">
          <NavLink href='/group1/calendar' icon={CalendarIcon} name='Calendar' />
          <NavLink href='/group1/members' icon={UsersIcon} name='Members' />
          <NavLink href='/myUserName' icon={SettingsIcon} name='Settings' />
        </div>
      </nav>
    </aside>
  );
}

const App: Component = () => {
  return (
    <div class="h-screen w-screen flex flex-col bg-white dark:bg-gray-900 dark:text-gray-200">
      <Header />
      <div class="flex grow">
        <Sidebar />
        <main class="p-4 overflow-auto grow">
          <Routes>
            <Route path="/feed" component={Feed} />
            <Route path="/group1/chat" component={Chat} />
            <Route path="/group1/members" component={Users} />
            {/* <Route path="/login" component={Login} />
            <Route path="/register" component={Register} /> */}
          </Routes>
        </main>
      </div>
    </div>
  );
};

export default App;
