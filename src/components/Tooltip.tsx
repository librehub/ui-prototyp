export function Tooltip(props: { position: string; children: any; title: string }) {
    return (
        <div
            data-tooltip-target="tooltip"
            class="relative inline-block"
        >
            {props.position === "top" && <>
                {props.children}
                <p
                    role="tooltip"
                    data-tooltip="tooltip"
                    class="absolute w-48 px-5 py-3 text-center text-gray-600 truncate -translate-x-1/2 bg-white rounded-lg shadow-lg -top-14 left-1/2 dark:shadow-none shadow-gray-200 dark:bg-gray-800 dark:text-white"
                >
                    {props.title}
                </p>
            </>}
            {props.position === "bottom" && <>
                {props.children}
                <p
                    role="tooltip"
                    data-tooltip="tooltip"
                    class="absolute w-48 px-5 py-3 text-center text-gray-600 truncate -translate-x-1/2 bg-white rounded-lg shadow-lg -bottom-12 left-1/2 dark:shadow-none shadow-gray-200 dark:bg-gray-800 dark:text-white"
                >
                    {props.title}
                </p>
            </>}
            {props.position === "left" && <>
                {props.children}
                <p
                    role="tooltip"
                    data-tooltip="tooltip"
                    class="absolute flex items-center justify-center w-48 p-3 text-gray-600 bg-white rounded-lg shadow-lg -left-[13.2rem] -top-4 dark:shadow-none shadow-gray-200 dark:bg-gray-800 dark:text-white"
                >
                    {props.title}
                </p>
            </>}
            {props.position === "right" && <>
                {props.children}
                <p
                    role="tooltip"
                    data-tooltip="tooltip"
                    class="absolute flex items-center justify-center w-48 p-3 text-gray-600 bg-white rounded-lg shadow-lg left-12 -top-4 dark:shadow-none shadow-gray-200 dark:bg-gray-800 dark:text-white"
                >
                    {props.title}
                </p>
            </>}
        </div>
    )
}
