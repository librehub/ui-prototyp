import { A } from "@solidjs/router";
import { Component } from "solid-js";

export function NavLink(props: {href: string; name: string; icon: Component; class?: string;}) {
  return (
      <A href={props.href} class={`p-1.5 text-gray-700 focus:outline-nones transition-colors duration-200 rounded-lg dark:text-gray-200 dark:hover:bg-gray-800 hover:bg-gray-100 ${props.class}`}>
        {props.icon({class: 'w-6 h-6'})}
        {/* <div>{name}</div> */}
      </A>
  )
}

export function NavButton(props: {func: () => void; icon: Component; name: string; class?: string;}) {
  return (
      <button onClick={props.func} class={`p-1.5 text-gray-700 focus:outline-nones transition-colors duration-200 rounded-lg dark:text-gray-200 dark:hover:bg-gray-800 hover:bg-gray-100 ${props.class}`}>
        {props.icon({class: 'w-6 h-6'})}
        {/* <div>{name}</div> */}
      </button>
  )
}
